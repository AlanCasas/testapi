//
//  Model.swift
//  TestApi
//
//  Created by Alan Casas on 26/1/17.
//  Copyright © 2017 Alan Casas. All rights reserved.
//

import Foundation
import UIKit

class Model{
    
    let id:String
    
    let title:String
    
    let image:UIImage
    
    let summary:String
    
    init(id:String, title:String, image:UIImage, summary:String) {
        
        self.id = id
        
        self.title = title
        
        self.image = image
        
        self.summary = summary
        
    }
    
}

