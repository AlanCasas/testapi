//
//  MainTableViewController.swift
//  TestApi
//
//  Created by Alan Casas on 26/1/17.
//  Copyright © 2017 Alan Casas. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    var array:[Model] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var movie:Model?
        
        let service = RemoteItunesMoviesService()
        
        service.getMovies { (results) in
            
            if let results = results{

                for arrayResults in results{
                    
                    for (_,_) in arrayResults{
                        
                        if let idTemp = arrayResults["id"], let titleTemp = arrayResults["title"], let imageTemp = arrayResults["image"], let summaryTemp = arrayResults["summary"]{
                            
                            if let imageData = NSData(contentsOf: URL(string: imageTemp)!){
                                
                                movie = Model(id: idTemp, title: titleTemp, image: UIImage(data: imageData as Data)!,summary: summaryTemp)
                                
                            }
                            
                        }
                        
                    }
                 
                    self.array.append(movie!)
                    
                    self.tableView.reloadData()
                    
                }
                
            }
            
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return array.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let movieToCell = array[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! MainTableViewCell

        cell.idLabel.text = movieToCell.id
        
        cell.titleLabel.text = movieToCell.title

        cell.imageCell.image = movieToCell.image
        
        return cell
        
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "detail"{
            
            let indexPath = self.tableView.indexPathForSelectedRow
            
            let movieToDetail = array[indexPath!.row]
            
            let destinationVC = segue.destination as! DetailViewController
            
            destinationVC.movieDetail = movieToDetail
            
        }
        
    }
    
}





























