//
//  DetailViewController.swift
//  TestApi
//
//  Created by Alan Casas on 27/1/17.
//  Copyright © 2017 Alan Casas. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imageDetail: UIImageView!
    
    @IBOutlet weak var textViewDetail: UITextView!
    
    var movieDetail:Model!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageDetail.image = movieDetail?.image
        
        textViewDetail.text = movieDetail?.summary
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
