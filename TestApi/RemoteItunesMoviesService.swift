//
//  RemoteItunesMoviesService.swift
//  TestApi
//
//  Created by Alan Casas on 26/1/17.
//  Copyright © 2017 Alan Casas. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RemoteItunesMoviesService{
    
    func getMovies(completionHandle: @escaping ([[String:String]]?) -> Void){
        
        let urlAPI = URL(string: "https://itunes.apple.com/es/rss/topmovies/limit=9/json")
        
        Alamofire.request(urlAPI!, method: .get).validate().responseJSON { (response) in
            
            switch response.result{
                
            case .success:

                if let value = response.value{
                    
                    let json = JSON(value)
                    
                    var results = [[String:String]]()
                    
                    let entries = json["feed"]["entry"].arrayValue
                    
                    for entry in entries{
                        
                        var movie = [String:String]()
                        
                        movie["id"] = entry["id"]["attributes"]["im:id"].stringValue
                        
                        movie["title"] = entry["im:name"]["label"].stringValue
                        
                        movie["image"] = entry["im:image"][0]["label"].stringValue.replacingOccurrences(of: "60x60", with: "200x200")
                        
                        movie["summary"] = entry["summary"]["label"].stringValue
                        
                        results.append(movie)
                        
                    }
                    
                    completionHandle(results)
                    
                }
                
                break
                
            case .failure(let error):
                
                print(error)
                
            }
            
        }
        
    }
    
}
